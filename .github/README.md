<h1 align="center"> 
  <img width="500" src="../docs/assets/logo.png" alt="Impulse">
</h1>

<h3 align="center">
Fully customizable, highly scalable, distributed, feature packed and<br>easy-to-use general puprose cloud server orchestration software
</h3>

<p align="center">
  
  <a href="https://discord.gg/d8JyMYa">
  <img alt="discord-server" 
       src="https://img.shields.io/discord/472896393540009984.svg?style=flat-square&logo=discord&colorA=%237289DA&colorB=%23555555&label=discord">  
  </a>

  <a href="https://gitlab.com/impulse-cl/impulse/commits/master">
  <img alt="gitlab-pipeline-status"
       src="https://gitlab.com/impulse-cl/impulse/badges/master/pipeline.svg">
  </a>
  
  <a href="https://github.com/impulse-cl/impulse/blob/master/LICENSE">
  <img alt="MIT-Licensed" 
       src="https://img.shields.io/github/license/mashape/apistatus.svg?style=flat-square">
  </a>
  
</p>

> <b>IMPORTANT NOTE:</b> Impulse is yet still in development. Therefore the README file might receive updates later on. Impulse can be used for testing etc. but is not ready for any enterprise environment.

<br>

Impulse is an open source cloud software for general purpose server orchestration mainly focussing 
on high scalability and availability, as well as customizability and being easy-to-use.
<br><br>
Simply put: Impulse allows you to manage your servers, sharing data across your network,
as well as, including but not limited to, automatically deploying servers using preconfigured
templates.

---

## Features
- Fully scalability and availability. Impulse is making use of the *[Raft Consensus Algorithm](https://raft.github.io/raft.pdf)* to provide high availability
- Developer friendly and consistent API, designed to be less failure prone


## Supporters 
![YourKit logo](https://www.yourkit.com/images/yklogo.png)  
YourKit is kindly supporting ImpulseCloud with its full-featured Java Profiler. 
YourKit, LLC is the creator of [YourKit Java Profiler](https://www.yourkit.com/java/profiler/)
and [YourKit .NET Profiler](https://www.yourkit.com/.net/profiler/),
innovative and intelligent tools for profiling Java and .NET applications.
