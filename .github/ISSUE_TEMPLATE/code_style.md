---
name: Code style & API design
about: Create a issue which covers code style and API design issues

---

**What bothers you?**
A clear and concise description what is bothering you.

**Code style / implementation alternatives**
A clear and concise description how a alternative would look like.
It would definitely be helpful providing code snippets.

**Additional context**
Add any other context about the issue here.
