package io.impulsecloud.daemon.node;

import io.impulsecloud.core.node.AbstractNode;
import io.impulsecloud.core.node.NodeType;
import io.impulsecloud.daemon.blueprint.collective.BlueprintCollective;
import io.impulsecloud.daemon.blueprint.factory.registry.BlueprintFactoryRegistry;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadataCollector;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadataInjector;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadataReader;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadataWriter;

public abstract class AbstractDaemonNode extends AbstractNode implements DaemonNode {

    private final Blueprints blueprints;

    public AbstractDaemonNode() {
        this(Blueprints.getDefault());
    }

    public AbstractDaemonNode(Blueprints blueprints) {
        this.blueprints = blueprints;
    }

    @Override
    public Blueprints blueprints() {
        return this.blueprints;
    }

    @Override
    public final NodeType type() {
        return NodeType.DAEMON;
    }

    @SuppressWarnings("NullableProblems")
    static final class DefaultBlueprints implements Blueprints {

        private BlueprintCollective collective;
        private BlueprintFactoryRegistry factoryRegistry;
        private BlueprintMetadataCollector metadataCollector;
        private BlueprintMetadataInjector metadataInjector;
        private BlueprintMetadataReader metadataReader;
        private BlueprintMetadataWriter metadataWriter;

        @Override
        public BlueprintCollective collective() {
            if (this.collective == null) {
                this.collective = BlueprintCollective.createDefault();
            }

            return this.collective;
        }

        @Override
        public BlueprintFactoryRegistry factoryRegistry() {
            if (this.factoryRegistry == null) {
                this.factoryRegistry = BlueprintFactoryRegistry.createDefault();
                BlueprintFactoryRegistry.setStandards(this.factoryRegistry);
            }

            return this.factoryRegistry;
        }

        @Override
        public BlueprintMetadataCollector metadataCollector() {
            if (this.metadataCollector == null) {
                this.metadataCollector = new BlueprintMetadataCollector();
            }

            return this.metadataCollector;
        }

        @Override
        public BlueprintMetadataInjector metadataInjector() {
            if (this.metadataInjector == null) {
                this.metadataInjector = new BlueprintMetadataInjector();
            }

            return this.metadataInjector;
        }

        @Override
        public BlueprintMetadataReader metadataReader() {
            if (this.metadataReader == null) {
                this.metadataReader = new BlueprintMetadataReader();
            }

            return this.metadataReader;
        }

        @Override
        public BlueprintMetadataWriter metadataWriter() {
            if (this.metadataWriter == null) {
                this.metadataWriter = new BlueprintMetadataWriter();
            }

            return this.metadataWriter;
        }

    }

}
