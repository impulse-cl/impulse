package io.impulsecloud.daemon.node;

import io.impulsecloud.core.node.Node;
import io.impulsecloud.daemon.blueprint.collective.BlueprintCollective;
import io.impulsecloud.daemon.blueprint.factory.registry.BlueprintFactoryRegistry;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadataCollector;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadataInjector;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadataReader;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadataWriter;
import io.impulsecloud.daemon.node.AbstractDaemonNode.DefaultBlueprints;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

public interface DaemonNode extends Node {

    Blueprints blueprints();

    String name();

    UUID uniqueId();

    interface Blueprints {

        BlueprintCollective collective();

        BlueprintFactoryRegistry factoryRegistry();

        BlueprintMetadataCollector metadataCollector();

        BlueprintMetadataInjector metadataInjector();

        BlueprintMetadataReader metadataReader();

        BlueprintMetadataWriter metadataWriter();

        static Path storage() {
            return Paths.get("blueprints").toAbsolutePath();
        }

        static Blueprints getDefault() {
            return new DefaultBlueprints();
        }

    }

}
