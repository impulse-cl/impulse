package io.impulsecloud.daemon.blueprint;

import java.nio.file.Path;

public interface Blueprint {

    String getType();

    void setDirectory(Path directory);

    Path getDirectory();

    Path getMetadataFile();

    void setName(String name);

    String getName();

    void setAuthor(String author);

    String getAuthor();

    void setVersion(String version);

    String getVersion();

}
