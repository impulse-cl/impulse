package io.impulsecloud.daemon.blueprint;

import io.impulsecloud.daemon.blueprint.factory.BlueprintFactory;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadata;

import com.google.common.base.Preconditions;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public abstract class AbstractBlueprint<T extends BlueprintFactory<? extends Blueprint>> implements Blueprint {

    @BlueprintMetadata
    private final String type;

    private Path directory;
    @BlueprintMetadata
    private String name;
    @BlueprintMetadata(required = false)
    private String author;
    @BlueprintMetadata(required = false)
    private String version;

    protected AbstractBlueprint(String type, T factory) {
        Preconditions.checkNotNull(factory, "'factory' cannot be null");
        Preconditions.checkNotNull(type, "'type' cannot be null");

        this.type = type;
        this.directory = factory.getDirectory();
        this.name = factory.getName();
        this.author = factory.getAuthor();
        this.version = factory.getVersion();
    }

    @Override
    public final void setDirectory(Path directory) {
        this.directory = directory;
    }

    @Override
    public final Path getDirectory() {
        return this.directory;
    }

    @Override
    public Path getMetadataFile() {
        return Paths.get(this.directory.toAbsolutePath().toString(), ".metadata");
    }

    @Override
    public final String getType() {
        return this.type;
    }

    @Override
    public final void setName(String name) {
        this.name = Preconditions.checkNotNull(name, "'name' cannot be null");
    }

    @Override
    public final String getName() {
        return this.name;
    }

    @Override
    public final void setAuthor(String author) {
        this.author = Preconditions.checkNotNull(author, "'author' cannot be null");
    }

    @Override
    public final String getAuthor() {
        return this.author;
    }

    @Override
    public final void setVersion(String version) {
        this.version = Preconditions.checkNotNull(version, "'version' cannot be null");
    }

    @Override
    public final String getVersion() {
        return this.version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AbstractBlueprint<?> that = (AbstractBlueprint<?>) o;
        return Objects.equals(this.getDirectory(), that.getDirectory())
            && Objects.equals(this.getType(), that.getType())
            && Objects.equals(this.getName(), that.getName())
            && Objects.equals(this.getAuthor(), that.getAuthor())
            && Objects.equals(this.getVersion(), that.getVersion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDirectory(), getType(), getName(), getAuthor(), getVersion());
    }

    @Override
    public String toString() {
        return "BaseBlueprint{"
            + "directory=" + directory.toAbsolutePath().toString()
            + ", type='" + type + '\''
            + ", name='" + name + '\''
            + ", author='" + author + '\''
            + ", version='" + version + '\''
            + '}';
    }

}
