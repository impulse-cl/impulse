package io.impulsecloud.daemon.blueprint;

import io.impulsecloud.daemon.blueprint.factory.DockerBlueprintFactory;

public class DockerBlueprint extends AbstractBlueprint<DockerBlueprintFactory> {

    public static String getTypeName() {
        return "docker";
    }

    public DockerBlueprint(DockerBlueprintFactory factory) {
        super(DockerBlueprint.getTypeName(), factory);
    }

}
