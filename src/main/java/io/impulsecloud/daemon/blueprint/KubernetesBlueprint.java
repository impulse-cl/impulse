package io.impulsecloud.daemon.blueprint;

import io.impulsecloud.daemon.blueprint.factory.KubernetesBlueprintFactory;

public class KubernetesBlueprint extends AbstractBlueprint<KubernetesBlueprintFactory> {

    public static String getTypeName() {
        return "kubernetes";
    }

    public KubernetesBlueprint(KubernetesBlueprintFactory factory) {
        super(KubernetesBlueprint.getTypeName(), factory);
    }

}
