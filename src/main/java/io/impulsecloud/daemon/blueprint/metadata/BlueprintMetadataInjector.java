package io.impulsecloud.daemon.blueprint.metadata;

import io.impulsecloud.daemon.blueprint.Blueprint;

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.Map;

public class BlueprintMetadataInjector extends BlueprintMetadataHelper {

    private static final Logger LOG = LogManager.getLogger(BlueprintMetadataCollector.class);

    public void inject(Blueprint target, Map<String, Object> source) {
        Preconditions.checkNotNull(target, "'target' cannot be null");
        Preconditions.checkNotNull(source, "'source' cannot be null");

        for (Field metadataField : super.collectMetadataFieldsAndGet(target.getClass())) {
            super.setAccessible(metadataField);

            BlueprintMetadata metadata = super.getMetadata(metadataField);
            String metadataKey = super.determineMetadataKey(metadataField, metadata);
            Object metadataValue = source.get(metadataKey);

            if (metadataValue != null) {
                this.injectIntoField(target, metadataField, metadataValue);
            } else if (metadata.required()) {
                LOG.error("Aborting metadata injection: Field '" + metadataField.getName() + "'"
                              + " with key '" + metadataKey + "': Is marked as required"
                              + " but value is not present");
                return;
            }
        }
    }

    private void injectIntoField(Blueprint target, Field targetField, Object value) {
        try {
            targetField.set(target, value);
        } catch (IllegalAccessException cause) {
            LOG.error("Failed assigning value to metadata field '" + targetField.getName() + "':", cause);
        }
    }

}
