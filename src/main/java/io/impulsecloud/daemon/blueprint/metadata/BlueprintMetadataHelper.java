package io.impulsecloud.daemon.blueprint.metadata;

import io.impulsecloud.daemon.blueprint.Blueprint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.collect.Lists;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public abstract class BlueprintMetadataHelper {

    private static final ObjectMapper MAPPER = new ObjectMapper(new YAMLFactory());

    protected static ObjectMapper getMapper() {
        return MAPPER;
    }

    protected BlueprintMetadataHelper() {}

    protected final Collection<Field> collectMetadataFieldsAndGet(Class<?> from) {
        Collection<Field> collectedFields = Lists.newArrayList();
        this.collectMetadataFields(collectedFields, from);

        return collectedFields;
    }

    protected final void collectMetadataFields(Collection<Field> collection, Class<?> target) {
        // Collect fields from (Base class) LegacyBlueprint and (Super class) AbstractBlueprint
        // Example: AbstractBlueprint -> DockerBlueprint -> AnotherBlueprint -> AnotherBlueprint2
        if (target.getSuperclass() != null && Blueprint.class.isAssignableFrom(target)) {
            this.collectMetadataFields(collection, target.getSuperclass());
        }

        collection.addAll(Arrays.stream(target.getDeclaredFields())
                              .filter(field -> field.isAnnotationPresent(BlueprintMetadata.class))
                              .collect(Collectors.toList()));
    }

    protected final void setAccessible(Field field) {
        field.setAccessible(true);

        try {
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            // TODO Dispose/Hide illegal reflective operation log
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        } catch (IllegalAccessException | NoSuchFieldException cause) {
            throw new IllegalStateException("Unknown error occurred while making field non final", cause);
        }
    }

    protected final String determineMetadataKey(Field metadataField, BlueprintMetadata metadata) {
        switch (metadata.key().trim()) {
            case "":
                return metadataField.getName();
            default:
                return metadata.key();
        }
    }

    protected final BlueprintMetadata getMetadata(Field metadataField) {
        return metadataField.getDeclaredAnnotation(BlueprintMetadata.class);
    }

}
