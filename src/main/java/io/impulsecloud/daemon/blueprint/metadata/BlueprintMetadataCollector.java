package io.impulsecloud.daemon.blueprint.metadata;

import io.impulsecloud.daemon.blueprint.Blueprint;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.Map;

public class BlueprintMetadataCollector extends BlueprintMetadataHelper {

    private static final Logger LOG = LogManager.getLogger(BlueprintMetadataCollector.class);

    public Map<String, Object> collect(Blueprint target) {
        Preconditions.checkNotNull(target, "'target' cannot be null");

        Map<String, Object> collectedMetadata = Maps.newLinkedHashMap();
        this.collect(target, collectedMetadata);

        return collectedMetadata;
    }

    public void collect(Blueprint target, Map<String, Object> destination) {
        Preconditions.checkNotNull(target, "'target' cannot be null");
        Preconditions.checkNotNull(destination, "'destination' cannot be null");

        for (Field metadataField : super.collectMetadataFieldsAndGet(target.getClass())) {
            super.setAccessible(metadataField);

            BlueprintMetadata metadata = super.getMetadata(metadataField);
            String metadataKey = super.determineMetadataKey(metadataField, metadata);
            Object metadataValue = this.resolveFieldValue(metadataField, target);

            // Failed resolving value of field; Moving on
            if (metadataValue == null) {
                continue;
            }

            destination.put(metadataKey, metadataValue);
        }
    }

    @SuppressWarnings("unchecked")
    private <T> T resolveFieldValue(Field field, Object instance) {
        try {
            return (T) field.get(instance);
        } catch (IllegalAccessException cause) {
            LOG.error("Failed resolving value of metadata field '" + field.getName() + "':", cause);
            return null;
        }
    }

}
