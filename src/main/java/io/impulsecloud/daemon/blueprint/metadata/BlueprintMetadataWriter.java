package io.impulsecloud.daemon.blueprint.metadata;

import io.impulsecloud.daemon.blueprint.Blueprint;

import com.google.common.base.Preconditions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class BlueprintMetadataWriter {

    private static final BlueprintMetadataCollector METADATA_COLLECTOR = new BlueprintMetadataCollector();

    public void toDirectory(Blueprint target) throws IOException {
        Preconditions.checkNotNull(target, "'target' cannot be null");

        this.toDirectory(target.getDirectory(), target);
    }

    public void toDirectory(Path directory, Blueprint target) throws IOException {
        Preconditions.checkNotNull(directory, "'directory' cannot be null");
        Preconditions.checkNotNull(target, "'target' cannot be null");

        if (Files.exists(directory)) {
            Preconditions.checkState(Files.isDirectory(directory), "Expected '"
                + directory.toAbsolutePath().toString() + "' to be a directory, not a file");
        }

        this.toFile(Paths.get(directory.toAbsolutePath().toString(), ".metadata"), target);
    }

    public void toFile(Path file, Blueprint target) throws IOException {
        Preconditions.checkNotNull(file, "'file' cannot be null");
        Preconditions.checkNotNull(target, "'target' cannot be null");

        if (Files.exists(file)) {
            Preconditions.checkState(!Files.isDirectory(file), "Expected '" + file.toAbsolutePath().toString()
                + "' to be a file, not a directory");
        } else {
            com.google.common.io.Files.createParentDirs(file.toFile());
        }

        Map<String, Object> collectedMetadata = METADATA_COLLECTOR.collect(target);
        BlueprintMetadataHelper.getMapper().writeValue(file.toFile(), collectedMetadata);
    }

}
