package io.impulsecloud.daemon.blueprint.metadata;

import com.google.common.base.Preconditions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class BlueprintMetadataReader {

    public Map<String, Object> fromDirectory(Path directory) throws IOException {
        Preconditions.checkNotNull(directory, "'directory' cannot be null");
        Preconditions.checkState(Files.isDirectory(directory), "Expected '" + directory.toAbsolutePath().toString()
            + "' to be a directory, not a file");

        return this.fromFile(Paths.get(directory.toAbsolutePath().toString(), ".metadata"));
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> fromFile(Path file) throws IOException {
        Preconditions.checkNotNull(file, "'file' cannot be null");
        Preconditions.checkState(!Files.isDirectory(file), "Expected '" + file.toAbsolutePath().toString()
            + "' to be a file, not a directory");

        return BlueprintMetadataHelper.getMapper().readValue(file.toFile(), Map.class);
    }

}
