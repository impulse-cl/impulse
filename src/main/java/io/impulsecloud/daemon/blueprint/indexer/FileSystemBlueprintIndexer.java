package io.impulsecloud.daemon.blueprint.indexer;

import io.impulsecloud.daemon.blueprint.collective.BlueprintCollective;
import io.impulsecloud.daemon.blueprint.factory.BlueprintFactory;
import io.impulsecloud.daemon.blueprint.factory.registry.BlueprintFactoryRegistry;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadataReader;

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;

public class FileSystemBlueprintIndexer implements BlueprintIndexer<Path> {

    private static final BlueprintMetadataReader METADATA_READER = new BlueprintMetadataReader();
    private static final Logger LOG = LogManager.getLogger(FileSystemBlueprintIndexer.class);

    @Override
    public void indexRecursive(Path directory, BlueprintFactoryRegistry factoryRegistry,
                               BlueprintCollective collective) {
        this.indexRecursive(directory, Integer.MAX_VALUE, factoryRegistry, collective);
    }

    @Override
    public void indexRecursive(Path directory, int recursiveDepth, BlueprintFactoryRegistry factoryRegistry,
                               BlueprintCollective collective) {
        Preconditions.checkNotNull(directory, "'directory' cannot be null");
        Preconditions.checkNotNull(factoryRegistry, "'factoryRegistry' cannot be null");
        Preconditions.checkNotNull(collective, "'collective' cannot be null");

        if (!Files.isDirectory(directory)) {
            LOG.warn("Aborting indexing: '" + directory.toAbsolutePath() + "'; Expected a directory, not a file");
            return;
        }

        try {
            Files.walk(directory, recursiveDepth)
                .filter(path -> Files.isDirectory(path))
                .filter(path -> !path.toAbsolutePath().equals(directory.toAbsolutePath()))
                // Check if the directory even has a '.metadata' file
                .filter(path -> Files.exists(Paths.get(path.toAbsolutePath().toString(), ".metadata")))
                .forEach(path -> this.index(path, factoryRegistry, collective));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void index(Path directory, BlueprintFactoryRegistry factoryRegistry, BlueprintCollective collective) {
        Preconditions.checkNotNull(directory, "'directory' cannot be null");
        Preconditions.checkNotNull(factoryRegistry, "'factoryRegistry' cannot be null");
        Preconditions.checkNotNull(collective, "'collective' cannot be null");

        if (!Files.isDirectory(directory)) {
            LOG.warn("Aborting indexing: '" + directory.toAbsolutePath() + "'; Expected a directory, not a file");
            return;
        }

        // Kind of pointless reading in the metadata file if no factory is available
        if (factoryRegistry.getFactoryClasses().isEmpty()) {
            LOG.warn("Aborting indexing: Passed blueprint factory is empty");
            return;
        }

        Path metadataFile = Paths.get(directory.toString(), ".metadata");

        LOG.debug("Scanning directory '" + directory.toAbsolutePath() + "' ...");

        if (!Files.exists(metadataFile)) {
            LOG.debug("Aborting indexing: No such file: " + metadataFile.toString());
            return;
        }

        Map<String, Object> metadata;

        try {
            metadata = METADATA_READER.fromFile(metadataFile);
        } catch (IOException cause) {
            LOG.error("Failed loading metadata from file '" + metadataFile.toString() + "'", cause);
            return;
        }

        String type = (String) metadata.get("type");

        if (type == null) {
            LOG.warn("Aborting indexing: Blueprint metadata lacks key 'type' (" + metadataFile.toString() + ")");
            return;
        }

        BlueprintFactory<?> factory;
        Optional<BlueprintFactory<?>> foundFactory = factoryRegistry.createFactory(type);

        if (foundFactory.isPresent()) {
            factory = foundFactory.get();
        } else {
            LOG.warn("Aborting indexing: No such blueprint factory for type '" + type + "'");
            return;
        }

        collective.add(factory.build(metadata));
    }

}
