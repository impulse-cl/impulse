package io.impulsecloud.daemon.blueprint.indexer;

import io.impulsecloud.daemon.blueprint.collective.BlueprintCollective;
import io.impulsecloud.daemon.blueprint.factory.registry.BlueprintFactoryRegistry;

import java.nio.file.Path;

public interface BlueprintIndexer<T> {

    void indexRecursive(T source, BlueprintFactoryRegistry factoryRegistry, BlueprintCollective collective);

    void indexRecursive(T source, int recursiveDepth, BlueprintFactoryRegistry factoryRegistry,
                        BlueprintCollective collective);

    void index(T source, BlueprintFactoryRegistry factoryRegistry, BlueprintCollective collective);

    static BlueprintIndexer<Path> createFileSystemIndexer() {
        return new FileSystemBlueprintIndexer();
    }

}
