package io.impulsecloud.daemon.blueprint.factory;

import io.impulsecloud.daemon.blueprint.LegacyBlueprint;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;

public class LegacyBlueprintFactory extends AbstractBlueprintFactory<LegacyBlueprint> {

    private String serverBinaryPath = Paths.get("path/to/binary").toAbsolutePath().toString();
    private List<String> envVars = Lists.newArrayList();
    private List<String> programArgs = Lists.newArrayList();

    public final LegacyBlueprintFactory setServerBinaryPath(String serverBinaryPath) {
        this.serverBinaryPath = Preconditions.checkNotNull(serverBinaryPath, "'serverBinaryPath' cannot be null");
        return this;
    }

    public final String getServerBinaryPath() {
        return this.serverBinaryPath;
    }

    public final LegacyBlueprintFactory addEnvVar(String name, String value) {
        Preconditions.checkNotNull(name, "'name' cannot be null");
        Preconditions.checkNotNull(value, "'name' cannot be null");

        this.envVars.add(name + "=" + value);
        return this;
    }

    public final List<String> getEnvVars() {
        return this.envVars;
    }

    public final LegacyBlueprintFactory setEnvVars(Collection<String> envVars) {
        Preconditions.checkNotNull(envVars, "'envVars' cannot be null");
        this.envVars = Lists.newArrayList(envVars);
        return this;
    }

    public final LegacyBlueprintFactory addProgramArg(String argument) {
        Preconditions.checkNotNull(argument, "'argument' cannot be null");
        this.programArgs.add(argument);
        return this;
    }

    public final List<String> getProgramArgs() {
        return this.programArgs;
    }

    public LegacyBlueprintFactory setProgramArgs(Collection<String> programArgs) {
        Preconditions.checkNotNull(programArgs, "'programArgs' cannot be null");
        this.programArgs = Lists.newArrayList(programArgs);
        return this;
    }

    @Override
    public final LegacyBlueprint build() {
        return new LegacyBlueprint(this);
    }

}
