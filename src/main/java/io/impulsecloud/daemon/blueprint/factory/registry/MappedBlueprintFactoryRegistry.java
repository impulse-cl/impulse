package io.impulsecloud.daemon.blueprint.factory.registry;

import io.impulsecloud.daemon.blueprint.factory.BlueprintFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class MappedBlueprintFactoryRegistry extends AbstractBlueprintFactoryRegistry {

    private static final Logger LOG = LogManager.getLogger(MappedBlueprintFactoryRegistry.class);

    private final Map<String, Class<? extends BlueprintFactory<?>>> factories;
    private boolean allowFactoryOverride;

    @SafeVarargs
    public MappedBlueprintFactoryRegistry(Pair<String, Class<? extends BlueprintFactory<?>>>... initial) {
        this(true, initial);
    }

    @SafeVarargs
    public MappedBlueprintFactoryRegistry(boolean allowFactoryOverride,
                                          Pair<String, Class<? extends BlueprintFactory<?>>>... initial) {
        this.factories = Maps.newHashMap();
        this.allowFactoryOverride = allowFactoryOverride;

        this.registerInitials(initial);
    }

    @Override
    public void setFactory(String type, Class<? extends BlueprintFactory<?>> factoryClass) {
        Preconditions.checkNotNull(type, "'type' cannot be null");
        Preconditions.checkNotNull(factoryClass, "'factoryClass' cannot be null");

        if (this.isFactoryAvailable(type) && !this.isFactoryOverrideAllowed()) {
            LOG.debug("Anonymous tried to override factory for type '" + type + "' (allowFactoryOverride="
                          + this.isFactoryOverrideAllowed() + ")");
            return;
        }

        this.factories.put(type, factoryClass);
    }

    @Override
    public boolean isFactoryAvailable(String type) {
        Preconditions.checkNotNull(type, "'type' cannot be null");
        return this.factories.containsKey(type);
    }

    @Override
    public Collection<Pair<String, Class<? extends BlueprintFactory<?>>>> getFactoryEntries() {
        return this.factories.entrySet().stream()
            .map(this.entryToPairFunction())
            .collect(Collectors.toList());
    }

    @Override
    public Collection<Class<? extends BlueprintFactory<?>>> getFactoryClasses() {
        return this.factories.values();
    }

    @Override
    public Optional<Class<? extends BlueprintFactory<?>>> getFactoryClass(String type) {
        Preconditions.checkNotNull(type, "'type' cannot be null");
        return Optional.ofNullable(this.factories.get(type));
    }

    public final boolean isFactoryOverrideAllowed() {
        return this.allowFactoryOverride;
    }

    @SafeVarargs
    private void registerInitials(Pair<String, Class<? extends BlueprintFactory<?>>>... initial) {
        for (Pair<String, Class<? extends BlueprintFactory<?>>> pair : initial) {
            if (pair.getKey() == null) {
                LOG.warn("Failed registering initial factory: 'type' (key) is null");
                continue;
            } else if (pair.getValue() == null) {
                LOG.warn("Failed registering initial factory: 'factory' (value) is null");
                continue;
            }

            this.setFactory(pair.getKey(), pair.getValue());
        }
    }

}
