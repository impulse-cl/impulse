package io.impulsecloud.daemon.blueprint.factory;

import io.impulsecloud.daemon.blueprint.DockerBlueprint;

public class DockerBlueprintFactory extends AbstractBlueprintFactory<DockerBlueprint> {

    @Override
    public DockerBlueprint build() {
        return new DockerBlueprint(this);
    }

}
