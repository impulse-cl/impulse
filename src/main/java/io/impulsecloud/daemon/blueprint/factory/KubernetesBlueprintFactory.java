package io.impulsecloud.daemon.blueprint.factory;

import io.impulsecloud.daemon.blueprint.KubernetesBlueprint;

public class KubernetesBlueprintFactory extends AbstractBlueprintFactory<KubernetesBlueprint> {

    @Override
    public KubernetesBlueprint build() {
        return new KubernetesBlueprint(this);
    }

}
