package io.impulsecloud.daemon.blueprint.factory;

import static io.impulsecloud.daemon.node.DaemonNode.Blueprints;

import io.impulsecloud.daemon.blueprint.Blueprint;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadataInjector;

import com.google.common.base.Preconditions;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.UUID;

@SuppressWarnings("NullableProblems")
public abstract class AbstractBlueprintFactory<B extends Blueprint> implements BlueprintFactory<B> {

    private Path directory;
    private String name;
    private String author;
    private String version;

    public AbstractBlueprintFactory() {
        this.setDirectory(this.getBlueprintDirectory(null));
        this.setName("unset");
        this.setAuthor("unknown");
        this.setVersion("0.0.1-SNAPSHOT");
    }

    @Override
    public final Path getDirectory() {
        return this.directory;
    }

    @Override
    public final AbstractBlueprintFactory<B> setDirectory(Path directory) {
        Preconditions.checkNotNull(directory, "Blueprint directory cannot be null");

        this.directory = directory;
        return this;
    }

    @Override
    public final String getName() {
        return this.name;
    }

    @Override
    public final AbstractBlueprintFactory<B> setName(String name) {
        this.name = Preconditions.checkNotNull(name, "'name' cannot be null");
        return this;
    }

    @Override
    public final String getAuthor() {
        return this.author;
    }

    @Override
    public final AbstractBlueprintFactory<B> setAuthor(String author) {
        this.author = Preconditions.checkNotNull(author, "'author' cannot be null");
        return this;
    }

    @Override
    public final String getVersion() {
        return this.version;
    }

    @Override
    public final AbstractBlueprintFactory<B> setVersion(String version) {
        this.version = Preconditions.checkNotNull(version, "'version' cannot be null");
        return this;
    }

    @Override
    public B build(Map<String, Object> metadata) {
        B blueprint = this.build();

        BlueprintMetadataInjector injector = new BlueprintMetadataInjector();
        injector.inject(blueprint, metadata);

        return blueprint;
    }

    protected final Path getBlueprintDirectory(String blueprintName) {
        if (blueprintName == null) {
            blueprintName = "Blueprint-" + UUID.randomUUID();
        }

        return Paths.get(Blueprints.storage().toString(), blueprintName);
    }

}
