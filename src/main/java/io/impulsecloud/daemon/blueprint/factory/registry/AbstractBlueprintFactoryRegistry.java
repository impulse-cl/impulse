package io.impulsecloud.daemon.blueprint.factory.registry;

import io.impulsecloud.daemon.blueprint.Blueprint;
import io.impulsecloud.daemon.blueprint.factory.BlueprintFactory;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;

public abstract class AbstractBlueprintFactoryRegistry implements BlueprintFactoryRegistry {

    private final Logger LOG = LogManager.getLogger(AbstractBlueprintFactoryRegistry.class);

    @Override
    public void mergeRegistries(BlueprintFactoryRegistry... registries) {
        for (BlueprintFactoryRegistry registry : registries) {
            this.mergeRegistry(registry);
        }
    }

    @Override
    public void mergeRegistry(BlueprintFactoryRegistry registry) {
        Preconditions.checkNotNull(registry, "'registry' cannot be null");

        for (Pair<String, Class<? extends BlueprintFactory<?>>> entry : registry.getFactoryEntries()) {
            //  Just for safety
            if (entry.getKey() == null) {
                LOG.warn("Failed merging registry entry: 'type' (key) is null");
                return;
            } else if (entry.getValue() == null) {
                LOG.warn("Failed merging registry entry: 'factory' (value) is null");
                return;
            }

            this.setFactory(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public <T extends BlueprintFactory<?>> Optional<T> createFactory(Blueprint blueprint) {
        Preconditions.checkNotNull(blueprint, "'blueprint' cannot be null");
        return this.createFactory(blueprint.getType());
    }

    @Override
    public <T extends BlueprintFactory<?>> Optional<T> createFactory(String type) {
        Preconditions.checkNotNull(type, "'type' cannot be null");

        if (!this.isFactoryAvailable(type)) {
            return Optional.empty();
        }

        //noinspection OptionalGetWithoutIsPresent / Already checked by the if-statement above
        Class<? extends BlueprintFactory<?>> factoryClass = this.getFactoryClass(type).get();

        if (factoryClass.isMemberClass() && !Modifier.isStatic(factoryClass.getModifiers())) {
            LOG.warn("'" + factoryClass.getName() + "': Class declaration of a inner "
                              + "class member is required to declare the 'static' modifier");
            return Optional.empty();
        }

        Constructor<? extends BlueprintFactory<?>> constructor;

        try {
            constructor = factoryClass.getConstructor();
        } catch (NoSuchMethodException cause) {
            LOG.error("Blueprint factories are required to declare a nullary constructor", cause);
            return Optional.empty();
        }

        try {
            //noinspection unchecked
            return (Optional<T>) Optional.of(constructor.newInstance());
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException cause) {
            LOG.error("Failed creating blueprint factory for type '" + type + "'", cause);
            return Optional.empty();
        }
    }

    @Override
    public void setFactory(Blueprint blueprint, Class<? extends BlueprintFactory<?>> factoryClass) {
        Preconditions.checkNotNull(blueprint, "'blueprint' cannot be null");
        Preconditions.checkNotNull(factoryClass, "'factory' cannot be null");

        this.setFactory(blueprint.getType(), factoryClass);
    }

    @Override
    public boolean isFactoryAvailable(Blueprint blueprint) {
        Preconditions.checkNotNull(blueprint, "'blueprint' cannot be null");
        return this.isFactoryAvailable(blueprint.getType());
    }

    @Override
    public Optional<Class<? extends BlueprintFactory<?>>> getFactoryClass(Blueprint blueprint) {
        Preconditions.checkNotNull(blueprint, "'blueprint' cannot be null");
        return this.getFactoryClass(blueprint.getType());
    }

    protected final Function<Entry<String, Class<? extends BlueprintFactory<?>>>,
        Pair<String, Class<? extends BlueprintFactory<?>>>> entryToPairFunction() {
        return entry -> Pair.of(entry.getKey(), entry.getValue());
    }

}
