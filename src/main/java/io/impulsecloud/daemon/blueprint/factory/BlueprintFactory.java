package io.impulsecloud.daemon.blueprint.factory;

import io.impulsecloud.daemon.blueprint.Blueprint;

import java.nio.file.Path;
import java.util.Map;

public interface BlueprintFactory<B extends Blueprint> {

    Path getDirectory();

    BlueprintFactory<B> setDirectory(Path directory);

    String getName();

    BlueprintFactory<B> setName(String name);

    String getAuthor();

    BlueprintFactory<B> setAuthor(String author);

    String getVersion();

    BlueprintFactory<B> setVersion(String version);

    B build();

    B build(Map<String, Object> metadata);

}
