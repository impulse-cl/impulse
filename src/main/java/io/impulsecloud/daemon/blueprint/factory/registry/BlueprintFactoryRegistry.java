package io.impulsecloud.daemon.blueprint.factory.registry;

import io.impulsecloud.daemon.blueprint.Blueprint;
import io.impulsecloud.daemon.blueprint.DockerBlueprint;
import io.impulsecloud.daemon.blueprint.KubernetesBlueprint;
import io.impulsecloud.daemon.blueprint.LegacyBlueprint;
import io.impulsecloud.daemon.blueprint.factory.BlueprintFactory;
import io.impulsecloud.daemon.blueprint.factory.DockerBlueprintFactory;
import io.impulsecloud.daemon.blueprint.factory.KubernetesBlueprintFactory;
import io.impulsecloud.daemon.blueprint.factory.LegacyBlueprintFactory;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.Optional;

public interface BlueprintFactoryRegistry {

    void mergeRegistries(BlueprintFactoryRegistry... registries);

    void mergeRegistry(BlueprintFactoryRegistry registry);

    <T extends BlueprintFactory<?>> Optional<T> createFactory(Blueprint blueprint);

    <T extends BlueprintFactory<?>> Optional<T> createFactory(String type);

    void setFactory(Blueprint blueprint, Class<? extends BlueprintFactory<?>> factoryClass);

    void setFactory(String type, Class<? extends BlueprintFactory<?>> factoryClass);

    boolean isFactoryAvailable(Blueprint blueprint);

    boolean isFactoryAvailable(String type);

    Collection<Pair<String, Class<? extends BlueprintFactory<?>>>> getFactoryEntries();

    Collection<Class<? extends BlueprintFactory<?>>> getFactoryClasses();

    Optional<Class<? extends BlueprintFactory<?>>> getFactoryClass(Blueprint blueprint);

    Optional<Class<? extends BlueprintFactory<?>>> getFactoryClass(String type);

    static BlueprintFactoryRegistry createDefault() {
        return new MappedBlueprintFactoryRegistry();
    }

    static void setStandards(BlueprintFactoryRegistry registry) {
        Preconditions.checkNotNull(registry, "'registry' cannot be null");

        registry.setFactory(DockerBlueprint.getTypeName(), DockerBlueprintFactory.class);
        registry.setFactory(KubernetesBlueprint.getTypeName(), KubernetesBlueprintFactory.class);
        registry.setFactory(LegacyBlueprint.getTypeName(), LegacyBlueprintFactory.class);
    }

}
