package io.impulsecloud.daemon.blueprint;

import io.impulsecloud.daemon.blueprint.factory.LegacyBlueprintFactory;
import io.impulsecloud.daemon.blueprint.metadata.BlueprintMetadata;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class LegacyBlueprint extends AbstractBlueprint<LegacyBlueprintFactory> {

    public static String getTypeName() {
        return "legacy";
    }

    @BlueprintMetadata
    private String serverBinaryPath;
    @BlueprintMetadata(key = "environmentVariables", required = false)
    private List<String> envVars;
    @BlueprintMetadata(key = "programArguments", required = false)
    private List<String> programArgs;

    public LegacyBlueprint(LegacyBlueprintFactory factory) {
        super(LegacyBlueprint.getTypeName(), factory);

        this.serverBinaryPath = factory.getServerBinaryPath();
        this.envVars = factory.getEnvVars();
        this.programArgs = factory.getProgramArgs();
    }

    public final void setServerBinaryPath(Path serverBinaryPath) {
        this.setServerBinaryPath(serverBinaryPath.toAbsolutePath().toString());
    }

    public final void setServerBinaryPath(String serverBinaryPath) {
        this.serverBinaryPath = Preconditions.checkNotNull(serverBinaryPath, "'serverBinaryPath' cannot be null");
    }

    public final String getServerBinaryPath() {
        return this.serverBinaryPath;
    }

    public final void setEnvVars(Collection<String> envVars) {
        Preconditions.checkNotNull(envVars, "'envVars' cannot be null");
        this.envVars = Lists.newArrayList(envVars);
    }

    public final List<String> getEnvVars() {
        return this.envVars;
    }

    public final void setProgramArgs(Collection<String> programArgs) {
        Preconditions.checkNotNull(programArgs, "'programArgs' cannot be null");
        this.programArgs = Lists.newArrayList(programArgs);
    }

    public final List<String> getProgramArgs() {
        return this.programArgs;
    }

    @Override
    public String toString() {
        return "LegacyBlueprint{"
            + "serverBinaryPath='" + serverBinaryPath + '\''
            + ", envVars=" + envVars
            + ", programArgs=" + programArgs
            + "} " + super.toString();
    }

}
