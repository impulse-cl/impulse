package io.impulsecloud.daemon.blueprint.collective;

import io.impulsecloud.daemon.blueprint.Blueprint;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface BlueprintCollective extends Set<Blueprint> {

    Collection<Blueprint> byType(String type);

    Collection<Blueprint> byAuthor(String author);

    Collection<Blueprint> byNames(String... names);

    Optional<Blueprint> byName(String name);

    static BlueprintCollective createDefault(BlueprintCollective merge) {
        return new StandardBlueprintCollective(merge);
    }

    static BlueprintCollective createDefault() {
        return new StandardBlueprintCollective();
    }

}
