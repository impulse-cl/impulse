package io.impulsecloud.daemon.blueprint.collective;

import io.impulsecloud.daemon.blueprint.Blueprint;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

public class StandardBlueprintCollective extends HashSet<Blueprint> implements BlueprintCollective {

    public StandardBlueprintCollective() {}

    public StandardBlueprintCollective(BlueprintCollective collective) {
        super(collective);
    }

    public StandardBlueprintCollective(Collection<? extends Blueprint> c) {
        super(c);
    }

    @Override
    public Collection<Blueprint> byType(String type) {
        Preconditions.checkNotNull(type, "'type' cannot be null");

        return this.stream()
            .filter(blueprint -> blueprint.getType().equalsIgnoreCase(type))
            .collect(Collectors.toList());
    }

    @Override
    public Collection<Blueprint> byAuthor(String author) {
        Preconditions.checkNotNull(author, "'author' cannot be null");

        return this.stream()
            .filter(blueprint -> blueprint.getAuthor().equalsIgnoreCase(author))
            .collect(Collectors.toList());
    }

    @Override
    public Collection<Blueprint> getByNames(String... names) {
        Collection<Blueprint> foundBlueprints = Sets.newHashSet();

        for (String name : names) {
            this.byName(name).ifPresent(foundBlueprints::add);
        }

        return foundBlueprints;
    }

    @Override
    public Optional<Blueprint> byName(String name) {
        return this.stream()
            .filter(blueprint -> blueprint.getName().equalsIgnoreCase(name))
            .findAny();
    }

}
