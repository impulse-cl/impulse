/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.filter.AbstractFilter;
import org.apache.logging.log4j.core.impl.MutableLogEvent;
import org.apache.logging.log4j.message.Message;

import java.util.Map;

public class ExceptionFilter extends AbstractFilter {

    private Map<Class<? extends Throwable>, String> excludes;

    public ExceptionFilter(Map<Class<? extends Throwable>, String> excludes) {
        super(Result.NEUTRAL, Result.NEUTRAL);
        this.excludes = excludes;
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
        final Object... params) {
        return Result.NEUTRAL;
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final Object msg,
        final Throwable t) {

        if (t != null) {
            if (shouldFiter(t)) {
                logger.log(level, marker, msg);
                return Result.DENY;
            }
        }

        return Result.NEUTRAL;
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final Message msg,
        final Throwable t) {

        if (t != null) {
            if (shouldFiter(t)) {
                logger.log(level, marker, msg);
                return Result.DENY;
            }
        }

        return Result.NEUTRAL;
    }

    @Override
    public Result filter(final LogEvent event) {
        final String text = event.getMessage().getFormattedMessage();

        if (event.getThrown() != null) {
            if (shouldFiter(event.getThrown())) {
                if (event.getMessage() instanceof MutableLogEvent) {
                    MutableLogEvent e = (MutableLogEvent) event.getMessage();
                    e.setThrown(null);
                    LogManager.getLogger(event.getLoggerName()).log(event.getLevel(), e);
                } else {
                    LogManager.getLogger(event.getLoggerName()).log(event.getLevel(), event.getMessage());
                }
                return Result.DENY;
            }
        }

        return Result.NEUTRAL;
    }

    private boolean shouldFiter(Throwable throwable) {
        if (excludes.containsKey(throwable.getClass())) {
            return throwable.getMessage().contains(excludes.get(throwable.getClass()));
        }
        return false;
    }
}
