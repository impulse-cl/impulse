/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.jar.Manifest;

/**
 * Small util class to allow interaction with the jars MANIFEST.MF
 *
 * @version 1.0
 * @since 1.0
 */
public class ManifestUtil {

    public static final String UNKNOWN = "UNKNOWN";

    private static final Logger LOG = LogManager.getLogger(ManifestUtil.class);

    /**
     * Loads the manifest file and returns the implementation version
     *
     * @return the implementation version
     */
    public static String getImplementationVersion() {
        Optional<Manifest> manifest = loadManifest();
        if (manifest.isPresent()) {
            return manifest.get().getMainAttributes().getValue("Implementation-Version");
        } else {
            return UNKNOWN;
        }
    }

    /**
     * Loads the manifest from the jar file (if run via jar) or from the output folder of your ide
     *
     * @return the loaded manifest
     */
    private static Optional<Manifest> loadManifest() {
        var clazz = ManifestUtil.class;
        var className = clazz.getSimpleName() + ".class";
        var classPath = clazz.getResource(className).toString();

        String manifestPath;

        if (classPath.contains("out/production")) {
            // running from intellij
            var relativePath = clazz.getName().replace('.', File.separatorChar) + ".class";
            var classFolder = classPath.substring(0, classPath.length() - relativePath.length() - 1);
            manifestPath = "file:/" + Paths.get(classFolder.replace("file:/", ""))
                // out of classes\
                .getParent()
                // into \resources\META-INF
                .resolve("resources").resolve("META-INF")
                // find manifest
                .resolve("MANIFEST.MF").toString();
            LOG.debug("manifestPath={}", manifestPath);
        } else if (!classPath.startsWith("jar")) {
            // Gradle running unit tests
            var relativePath = clazz.getName().replace('.', File.separatorChar) + ".class";
            var classFolder = classPath.substring(0, classPath.length() - relativePath.length() - 1);
            manifestPath = "file:/" + Paths.get(classFolder.replace("file:/", ""))
                // out of classes\java\main\ or
                .getParent().getParent().getParent()
                // into \resources\main\META-INF
                .resolve("resources").resolve("main").resolve("META-INF")
                // find manifest
                .resolve("MANIFEST.MF").toString();
            LOG.debug("manifestPath={}", manifestPath);
        } else {
            // Class was loaded from jar
            manifestPath = classPath.substring(0, classPath.lastIndexOf("!") + 1) + "/META-INF/MANIFEST.MF";
            LOG.debug("manifestPath={}", manifestPath);
        }

        try {
            return Optional.of(new Manifest(new URL(manifestPath).openStream()));
        } catch (IOException e) {
            LOG.error("Error while trying to read manifest", e);
            return Optional.empty();
        }
    }

}
