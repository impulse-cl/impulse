/*
 *
 *  * Copyright (c) 2018, Impulse Cloud and its contributors
 *  *
 *  * This code is licensed under the MIT license found in the
 *  * LICENSE file in the root directory of this source tree.
 *
 */

package io.impulsecloud.util;

import joptsimple.ValueConverter;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Collection of {@link ValueConverter}s, used to do type save command line parsing
 *
 * @version 1.0
 * @since 1.0
 */
public class ValueConverters {

    public static final ValueConverter<Path> PATH = new ValueConverter<>() {
        @Override
        public Path convert(String input) {
            return Paths.get(input);
        }

        @Override
        public Class<? extends Path> valueType() {
            return Path.class;
        }

        @Override
        public String valuePattern() {
            return null;
        }
    };

}
