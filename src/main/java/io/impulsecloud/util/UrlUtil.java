package io.impulsecloud.util;

import com.google.common.base.Preconditions;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;

public final class UrlUtil {

    public static URL createUrlSneaky(String context) {
        Preconditions.checkNotNull(context, "URL context cannot be null");

        try {
            return new URL(context);
        } catch (MalformedURLException cause) {
            throw new IllegalStateException(cause);
        }
    }

    public static URL asUrl(Path context) {
        Preconditions.checkNotNull(context, "URL context cannot be null");

        try {
            return context.toAbsolutePath().toUri().toURL();
        } catch (MalformedURLException cause) {
            throw new IllegalStateException(cause);
        }
    }

    private UrlUtil() {}

}
