package io.impulsecloud.core.event.bus;

import io.impulsecloud.core.event.Event;

public interface EventBus {

    <T extends Event> void post(T... events);

    <T extends Event> void post(T event);

}
