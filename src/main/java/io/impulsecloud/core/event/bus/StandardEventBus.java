package io.impulsecloud.core.event.bus;

import io.impulsecloud.core.event.Event;
import io.impulsecloud.core.event.handler.EventHandlerContainer;
import io.impulsecloud.core.event.handler.EventHandlerRegistry;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

import java.util.Optional;

public class StandardEventBus implements EventBus {

    private final EventHandlerRegistry handlerRegistry;

    public StandardEventBus(EventHandlerRegistry handlerRegistry) {
        this.handlerRegistry = handlerRegistry;
    }

    @Override
    public <T extends Event> void post(T... events) {
        Preconditions.checkNotNull(events, "Passed array of events is referring to a null reference");

        for (T event : events) {
            this.post(event);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends Event> void post(T event) {
        Preconditions.checkNotNull(event, "Passed event is referring to a null reference");

        Optional<EventHandlerContainer<T>> container = this.handlerRegistry.getContainer((Class<T>) event.getClass());
        container.ifPresent(foundContainer -> foundContainer.post(event));
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(handlerRegistry);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StandardEventBus that = (StandardEventBus) o;
        return Objects.equal(handlerRegistry, that.handlerRegistry);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("handlerRegistry", handlerRegistry)
            .toString();
    }

}
