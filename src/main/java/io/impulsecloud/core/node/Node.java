package io.impulsecloud.core.node;

import io.impulsecloud.core.event.bus.EventBus;

public interface Node {

    void start();

    void stop();

    EventBus eventBus();

    NodeType type();

}
