package io.impulsecloud.core.node;

public enum NodeType {

    DAEMON,
    SWARM,
    CLIENT

}
